""" Template for Project 1: Morse code """

# TODO: Se på de siste inputene når knappen er holdt inne for å forhindre at random sender en 0 som slipper opp knappen

#Imports
import keyboard
import time

from GPIOSimulator_v1 import *
GPIO = GPIOSimulator()

MORSE_CODE = {'.-': 'a', '-...': 'b', '-.-.': 'c', '-..': 'd', '.': 'e', '..-.': 'f', '--.': 'g',
              '....': 'h', '..': 'i', '.---': 'j', '-.-': 'k', '.-..': 'l', '--': 'm', '-.': 'n',
              '---': 'o', '.--.': 'p', '--.-': 'q', '.-.': 'r', '...': 's', '-': 't', '..-': 'u',
              '...-': 'v', '.--': 'w', '-..-': 'x', '-.--': 'y', '--..': 'z', '.----': '1',
              '..---': '2', '...--': '3', '....-': '4', '.....': '5', '-....': '6', '--...': '7',
              '---..': '8', '----.': '9', '-----': '0'}

#

class MorseDecoder():
    """ Morse code class """

    def __init__(self):
        """ initialize your class """
        self.current_signal = ""
        self.current_word = ""
        self.current_sentence = ""
        self.T = 0.2
        self.tid = 0
        self.signals = ""


    def reset(self):
        """ reset the variable for a new run """
        GPIO.cleanup()
        GPIO.setup(PIN_BTN, 1)
        GPIO.setup(PIN_RED_LED_0, 0)
        GPIO.setup(PIN_RED_LED_1, 0)
        GPIO.setup(PIN_RED_LED_2, 0)
        GPIO.setup(PIN_BLUE_LED, 0)

    def read_one_signal(self):
        """ read a signal from Raspberry Pi """
        if(GPIO.pin_states[PIN_BLUE_LED]):
            signal = "-"
        elif(GPIO.pin_states[PIN_RED_LED_0]):
            signal = "."
        else:
            signal = "2"
        return signal


    # helt til knappen er trykket inn så ønsker jeg å ta tiden, hvis det tar T før den er trykket og mindre enn 7T så vil
    # den gi signal 2, hvis det er lengere enn 7T gir jeg signal 3. Må ha en måte og ta tiden på hvor lenge den ble holdt inne
    # for å kunne gi signal . og -

    def decoding_loop(self):
        """ the main decoding loop """
        # Bruker start_time og end time for å kunne ha en kontinuelig klokke som teller slik at man får ny bokstav eller nytt ord
        # Disse tidene resettes dersom man lager en - eller en .
        # Mens signalet til knappen er høyt så lager vi en ny klokke som sjekker hvor lenge knappen har blitt holdt inne
        # etter hvert ord er ferdig ønsker vi å vise det til consollen

        start_time = time.time()
        print("Begyn å skriv")
        while True:
            # Kjører en reset for å sette opp portene
            self.reset()
            end_time = time.time()
            tid = end_time - start_time
            # Hvis det har gått 7T lag et mellomrom
            if tid > 7*self.T and self.current_word != "":
                self.process_signal("3")
                print("ferdig ord")
                self.show_message()
                start_time = time.time()

            # Hvis det har gått 3T lag en ny bokstav
            if tid > 3*self.T and tid < 7*self.T and self.current_signal != "":
                start_time = time.time()
                print("ny bokstav")
                self.process_signal("2")

            # Mens knappen er nede, oppdater tiden
            press_start = time.time()
            tid_trykket = 0
            if GPIO.input(PIN_BTN) == 1:
                self.signals += str(GPIO.input(PIN_BTN))
                while self.signals[-10:] != "0000000000":
                    self.signals += str(GPIO.input(PIN_BTN))
                    press_end = time.time()
                    tid_trykket = press_end - press_start
                    if self.signals[:2] != "11":
                        tid_trykket = 0
                        break
                self.signals = ""

            # Håndterer et langt trykk
            if tid_trykket > 3*self.T:
                GPIO.output(PIN_BLUE_LED, GPIO.HIGH)
                self.process_signal(self.read_one_signal())
                self.reset()
                start_time = time.time()

            # Sjekker om tiden knappen ble trykket er mindre enn et blått signal, og større en et kjempekort et, dette er for å forhindre random trykk
            if tid_trykket < 3*self.T and tid_trykket > 0 and GPIO.input(PIN_BTN) != 1:
                GPIO.output(PIN_RED_LED_0, GPIO.HIGH)
                self.process_signal(self.read_one_signal())
                self.reset()
                start_time = time.time()


    def process_signal(self, signal):
        """ handle the signals using corresponding functions """
        if(signal == "." or signal == "-"):
            self.update_current_symbol(signal)
        elif signal == "2":
            self.handle_symbol_end()
        else:
            self.handle_word_end()
    def update_current_symbol(self, signal):
        """ append the signal to current symbol code """
        self.current_signal += signal
        self.current_signal

    def handle_symbol_end(self):
        """ process when a symbol ending appears """
        #Velger å ha en for løkke for å kunne korrigere dersom man har fått en dott for mye
        for letter in MORSE_CODE:
            if self.current_signal == letter:
                self.current_word += MORSE_CODE[letter]
                self.current_signal = ""

    def handle_word_end(self):
        """ process when a word ending appears """
        #Når et ord er ferdig legger man den til i setningen og setter ordet til 0
        self.current_sentence += self.current_word + " "
        self.current_word = ""
        self.current_signal = ""
    def handle_reset(self):
        """ process when a reset signal received """

    def show_message(self):
        """ print the decoded message """
        print(self.current_sentence)

def main():
    """ the main function """
    morse = MorseDecoder()
    morse.decoding_loop()


if __name__ == "__main__":
    main()
